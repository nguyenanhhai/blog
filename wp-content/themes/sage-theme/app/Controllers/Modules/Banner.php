<?php

namespace App\Controllers\Modules;

// Nguyễn Anh Hải
class Banner
{
    public function dataModule($module)
    {
        return (object) [
            // 'module' => $module,
            'image' => $module['image'],  
            'caption' => $module['caption'],
            // 'logo' => get_field('ns_header_logo', ACF_OPTION)
        ];
    }
}
