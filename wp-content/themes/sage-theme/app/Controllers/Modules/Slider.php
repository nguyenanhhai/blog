<?php

namespace App\Controllers\Modules;

// Nguyễn Anh Hải
class Slider
{
    public function dataModule($module)
    {
        return (object) [
            // 'module' => $module,
            'images' => array_map(function($image) {
              return (object) [
                "url" => $image->url,
                "title" => $image->title,
                "caption" => $image->caption
              ];
            }, $module['images']),  
            // 'logo' => get_field('ns_header_logo', ACF_OPTION)
        ];
    }
}
