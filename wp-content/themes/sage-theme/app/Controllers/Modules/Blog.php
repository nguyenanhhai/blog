<?php

namespace App\Controllers\Modules;

use App\Services\Queries;

// Nguyễn Anh Hải
class Blog
{
    public function dataModule($module)
    {
        return (object) [
            // 'module' => $module,
            'title' => $module['title'],
            'blogs' => $this->getBlogs(),
            // 'logo' => get_field('ns_header_logo', ACF_OPTION)
        ];
    }

    protected function getBlogs() {
      return Queries::getBlogs();
    }
}
