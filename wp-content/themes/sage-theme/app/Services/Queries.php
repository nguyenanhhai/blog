<?php

namespace App\Services;

class Queries
{
    public static function testExample()
    {
        $args = [
            'post_type' => 'post'
        ];
        $queryAll = new \WP_Query($args);
        return [
            'posts' => $queryAll->post
        ];
    }


    // Nguyễn Anh Hải
    public static function getBlogs() {
        $args = [
            'post_type' => 'blog'
        ];

        $query = new \WP_Query($args);

        return array_map(function($blog) {
            return (object) [
                "id" => $blog->ID,
                "header" => \get_field("header", $blog->ID),
                "content" => apply_filters('content_excerpt', (object) [
                    "blogId" => $blog->ID,
                    "text" => \get_field("content", $blog->ID),
                ])
            ];
        }, $query->posts);
    }

    // Nguyễn Anh Hải
    public static function getBlogContentById($blogId) {
        $args = [
            'post_type' => 'blog',
            'p' => $blogId
        ];

        $query = new \WP_Query($args);

        $blog = $query->post;
        if ($blog) {
            return \get_field("content", $blog->ID);
        }
        return null;
    }
}
