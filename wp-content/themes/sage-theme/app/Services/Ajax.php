<?php

namespace App\Services;

class Ajax
{
    public function init()
    {
        // Setup Ajax action hook
        // Nguyễn Anh Hải
        add_action('wp_ajax_blog_read_more', array($this, 'blogReadMore'));
        add_action('wp_ajax_nopriv_blog_read_more', array($this, 'blogReadMore'));

        add_action('wp_ajax_get_example', array($this, 'getExample'));
        add_action('wp_ajax_nopriv_get_example', array($this, 'getExample'));


    }
    public function getExample()
    {
        $website = (isset($_POST['website'])) ? esc_attr($_POST['website']) : '';
        $content = Helpers::ajaxRender('test-ajax.blade.php', [
            'key1' => 'ok1',
            'key2' => 'ok2',
        ]);
        wp_send_json_success([
            'text' => 'Chào mừng bạn đến với ' . $website,
            'value' => $content,
        ]);
    }

    // Nguyễn Anh Hải
    public function blogReadMore() {
        $blogId = (int)$_GET['blogId'] ?? '';

        $content = Queries::getBlogContentById($blogId);

        if ($content) {
            // $contentHtml = Helpers::ajaxRender('blog-content-ajax.blade.php', [
            //     'content' => $content
            // ]);
            
            wp_send_json_success([
                // 'contentHtml' => $contentHtml,
                'contentHtml' => $content
            ]);
        } else {
            wp_send_json_error( [
                'message' => 'Blog not found'
            ], 404 );
        }
    }
}
