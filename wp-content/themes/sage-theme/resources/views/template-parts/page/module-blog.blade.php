{{-- Nguyễn Anh Hải --}}

<!-- module 'Mod Blog' -->
<section class="module mod-blog">
  <div class="container">
    <h2 class="text-primary text-center">{{$data->title}}</h2>
    @foreach ($data->blogs as $blog)
      <div class="space-40"></div>
      <div class="blog">
        {{ Page::filterBlogHeader($blog->header) }}
        <div class="blog-content">{!! $blog->content !!}</div>
      </div>
    @endforeach
  </div>
</section>