{{-- Nguyễn Anh Hải --}}

<!-- module 'Mod Banner' -->
<section class="module mod-banner lazy d-flex bg-img" data-src="{{ $data->image }}">
  <div class="container">
    <div>
      <h1 class="heading text-center text-primary">{{$data->caption}}</h1>
    </div>
  </div>
</section>