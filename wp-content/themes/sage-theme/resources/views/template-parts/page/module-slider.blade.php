<!-- module 'Mod Slider' -->

{{-- Nguyễn Anh Hải --}}
<section class="module mod-slider">
  <div class="container-fluid px-0">
    <div class="slider slider-lazy has-slider">
      {{-- @dump($data) --}}
      @foreach ($data->images as $image)
        <div class="slider-item">
          <div class="slider-img-block">
            <img class="lazy slider-img img" data-src="{{ $image->url }}" src="{{IMG_BASE64}}" alt="{{$image->title}}">
          </div>
          <div class="slider-caption font-weight-bold text-primary text-center">
            {{$image->caption}}
          </div>
        </div>
      @endforeach
      
    </div>
  </div>
</section>