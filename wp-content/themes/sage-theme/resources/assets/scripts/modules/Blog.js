// Nguyễn Anh Hải

const internals = {
  /**
   * Variable
   */
  $this: $('.read-more'),
  /**
   * Function
   */
  addEventReadMore() {
    this.$this.on('click', function() {
      $.ajax({
        type: 'get',
        dataType: 'json',
        url: window.AJAX_URL,
        data: {
          action: 'blog_read_more',
          blogId: this.dataset.blogId
        },
        context: this,
        success(res) {
          if (res.success) {
            // alert(res.data.contentHtml)
            $(this).parent().html(res.data.contentHtml)
          }
        },
        error(jqXHR, textStatus) {
          console.error('The following error occured: ' + textStatus, jqXHR.responseJSON.data.message);
        }
      })
    });
  }
}

const Blog = (() => {
  if (internals.$this.length) {
    internals.addEventReadMore()
  }
})()

export default Blog